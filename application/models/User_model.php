<?php
class User_model extends CI_Model {
	
		public $table = 'users';
		public $sort = 'id';
		public $order = 'DESC';
	
    public function get_all()
		{
			$result = $this->db->where('role','1')->order_by($this->sort,$this->order)->get($this->table)->result();
			if($result)
				return $result;
			else
				return false;
    }
	
    public function get($id)
		{
			$result = $this->db->where('id',$id)->get($this->table)->row();
			if($result)
				return $result;
			else
				return false;
    }
	
    public function get_mark($id)
		{
			$result = $this->db->where('user_id',$id)->like('created_at',date('d-m-Y'))->get('attendance')->row();
			if($result){
				if($result->mark_in == 1 && $result->mark_out == 1){
				$mark = 'Todays Marked Done';
				}elseif($result->mark_in == 1 && $result->mark_out == 0){
					$mark = 'Mark Out';
				}
			}else{
				$mark = 'Mark In';
			}
			return $mark;
    }
	
    public function mark_in_out($id)
		{
			$current_status = $this->db->where('user_id',$id)->like('created_at',date('d-m-Y'))->get('attendance')->row();
			if($current_status){
				if($current_status->mark_in == 1 && $current_status->mark_out == 1){
					$mark = 'marked';
				}elseif($current_status->mark_in == 1 && $current_status->mark_out == 0){
					$mark = 'out';
				}
			}else{
				$mark = 'in';
			}
			if($mark == 'in'){
					$data['user_id'] 		= $id;
					$data['hod'] 		= get_type_by_id('users','id',$id,'hod');
					$data['mark_in'] 		= 1;
					$data['created_at'] = date('d-m-Y H:i:s');
					$this->db->insert('attendance',$data);
					return 'mark in';
			}elseif($mark == 'out'){
					$this->db->set('mark_out',1)->where('user_id',$id)->update('attendance');
					return 'mark out';
			}elseif($mark == 'marked'){
					return 'both marked';
			}else{}
    }

}