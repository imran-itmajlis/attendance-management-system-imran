<?php
class Attendance_model extends CI_Model {
	
		public $table = 'attendance';
		public $sort = 'id';
		public $order = 'DESC';
	
    public function get_all($hod_id)
		{
			$result = $this->db->where('hod',$hod_id)->order_by($this->sort,$this->order)->get($this->table)->result();
			if($result)
				return $result;
			else
				return false;
    }
	
}