<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function check_login(){
		$CI = get_instance();
		if ($CI->session->userdata('is_login') != TRUE) {
				$CI->session->sess_destroy();
				redirect(base_url());
		}
	}
	
	
	function get_type_by_id($table,$field,$id,$name)
	{
		$CI = get_instance();
		$result = $CI->db->select($name)->where($field,$id)->get($table)->row();
		return $result->$name;
	}
