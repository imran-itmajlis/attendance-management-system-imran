<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Attendance Management System</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<header class="d-none">
<div class="header-nav">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <a href="#" class="navbar-brand">Attendance</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Profile</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Messages</a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Inbox</a>
                        <a href="#" class="dropdown-item">Drafts</a>
                        <a href="#" class="dropdown-item">Sent Items</a>
                        <div class="dropdown-divider"></div>
                        <a href="#"class="dropdown-item">Trash</a>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav ml-auto">
							<li class="nav-item dropdown">
									<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
									<div class="dropdown-menu dropdown-menu-right">
											<a href="#" class="dropdown-item">Reports</a>
											<a href="#" class="dropdown-item">Settings</a>
											<div class="dropdown-divider"></div>
											<a href="#"class="dropdown-item">Logout</a>
									</div>
							</li>
            </ul>
        </div>
    </nav>
</div>
</header>
		<div class="container">
			<div class="row">
				<div class="wrapper form-wrapper">
					<?php $alert = $this->session->flashdata();
						if($alert){ ?>
							<div style="width:100%" class="alert alert-<?= $alert['message']['class']; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?= $alert['message']['info']; ?>
						</div>
					<?php } ?>
					<div id="formContent">
						<?php echo validation_errors(); ?>
						<form action="<?php echo base_url('auth/check'); ?>" method="post">
							<h2 class="box-title m-b-40 text-center">Login</h2>
							<div class="form-group ">
									<div class="col-xs-12">
											<input class="form-control" type="email" name="user_name" required="" placeholder="Username"> </div>
							</div>
							<div class="form-group">
									<div class="col-xs-12">
											<input class="form-control" type="password" name="password" required="" placeholder="Password"> 
									</div>
							</div><br>
							<div class="form-group">
									<div class="col-md-12">
											<a href="<?php echo base_url('admin/pages/recover') ?>" id="to-recover" class="text-dark pull-right"><!-- <i class="fa fa-lock m-r-5"></i> Forgot password? --></a>
									</div>
							</div>

							<!-- CSRF token -->
							<input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

							<div class="form-group text-center m-t-50">
									<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
									</div>
							</div>

							<div class="form-group m-b-0">
								<div class="col-sm-12 text-center">
									<p>Don't have an account? <a href="<?php echo base_url('/register') ?>" class="text-info m-l-5"><b>Register Now</b></a></p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>