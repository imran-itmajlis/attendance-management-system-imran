<nav class="navbar navbar-expand-md navbar-light bg-light">
        <a href="#" class="navbar-brand">Attendance Management System</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><?= ucwords($this->session->userdata('name')); ?></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?= base_url(); ?>auth/logout"class="dropdown-item">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>