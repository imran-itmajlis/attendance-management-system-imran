<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Attendance Management System</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="wrapper form-wrapper">
					<div id="formContent">
						<div class="errors"><?php echo validation_errors(); ?></div>
						<form action="" method="POST" enctype="multipart/form-data">
							<h2 class="box-title m-b-40 text-center">Register</h2>
							<div class="form-group " id="user_role_main">
								<div class="col-xs-12">
									<select class="form-control" id="user_role" name="role" required="">
										<option value="">--Please Select Role--</option>
										<option value="1">HOD</option>
										<option value="2">Employee</option>
									</select>
								</div>
							</div>
							<div class="form-group " id="list_hod">
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="text" name="name" required="" placeholder="Name"> 
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="email" name="email" required="" placeholder="Email"> 
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="number" name="mobile" required="" placeholder="Mobile No"> 
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="file" name="profile_img"  placeholder="Profile Image"> 
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="password" name="password" required="" placeholder="Password"> 
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="password" name="passconf" required="" placeholder="Confirm Password"> 
								</div>
							</div>
							<!-- CSRF token -->
							<input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
							<div class="form-group text-center m-t-50">
								<div class="col-xs-12">
									<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit">Register</button>
								</div>
							</div>
							
							<div class="form-group m-b-0">
								<div class="col-sm-12 text-center">
									<p><a href="<?php echo base_url() ?>" class="text-info m-l-5"><b>Login Now</b></a></p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script>
		$( '#user_role').change(function() {
			$role = $('#user_role').val();
			if($role == 2){
				$('#list_hod').append(
								'<div class="col-xs-12" id="hod_main">'+
									'<select class="form-control" id="hod" name="hod" required="">'+
										'<option value="">--Please Select HOD--</option>'+
										<?php foreach($hod_users as $hod_user){ ?>
										'<option value="<?= $hod_user->id; ?>"><?= $hod_user->name; ?></option>'+
										<?php } ?>
									'</select>'+
								'</div>');
			}else{
				$('#hod_main').remove();
			}
		});
		</script>
	</body>
</html>