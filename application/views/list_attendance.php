<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
  <h4>List of Attendance for Approval</h4>            
  <table class="table table-striped datatable">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Mobile No</th>
        <th>Mark In</th>
        <th>Mark Out</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
		<?php foreach($attendance as $row){ var_dump($row->user_id); ?>
      <tr>
        <td><?= get_type_by_id('users','id',$row->user_id,'name'); ?></td>
        <td><?= get_type_by_id('users','id',$row->user_id,'email'); ?></td>
        <td><?= get_type_by_id('users','id',$row->user_id,'mobile'); ?></td>
        <td><?= $row->mark_in; ?></td>
        <td><?= $row->mark_out; ?></td>
      </tr>
		<?php } ?>
    </tbody>
  </table>
</div>
 <script>
  $(function(){
    $(".datatable").dataTable();
  })
  </script>