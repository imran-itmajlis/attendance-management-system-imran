<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
	margin-bottom: 15px;
}

.title {
  color: grey;
  font-size: 18px;
}

.card a {
  border: none;
	text-decoration:none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #0dc570;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

.card a:hover, a:hover {
  opacity: 0.7;
}

a:not([href]):not([tabindex]) {
    color: #e6e6e6;
    cursor: no-drop;
    text-decoration: none;
}

a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
    color: #e6e6e6;
    cursor: no-drop;
    text-decoration: none;
}
</style>
<h3>Mark Attendance</h3>

	<div class="card">
		<img src="<?= base_url().'uploads/'; ?><?= !empty($user->profile_img) ? $user->profile_img : 'default-profile.jpg'; ?>" alt="John" style="width:100%">
		<h1><?= ucwords($user->name); ?></h1>
		<p class="title">HOD - <?= get_type_by_id('users','id',$user->hod,'name'); ?></p>
		<?php if($mark == 'Todays Marked Done'){ ?>
		<a class="disabled" ><?= $mark; ?></a>
		<?php }else{ ?>
		<a href="<?= base_url(); ?>dashboard/mark_in_out?user_id=<?= $user->id; ?>" ><?= $mark; ?></a>
		<?php } ?>
	</div>
		<?php $alert = $this->session->flashdata();
		if($alert){ ?>
			<div class="alert alert-<?= $alert['message']['class']; ?> alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?= $alert['message']['info']; ?>
		</div>
		<?php } ?>