<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


	public function __construct(){
			parent::__construct();
			$this->load->model('login_model');
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function check(){
			if($_POST){ 
					$row = $this->login_model->validate_user();
					//-- if valid
					if($row){
							$data = array(
									'id' => $row->id,
									'name' => $row->name,
									'email' =>$row->email,
									'role' =>$row->role,
									'is_login' => TRUE
							);
							$this->session->set_userdata($data);
							if($row->role == 1){
								redirect(base_url().'dashboard/list_attendance');
							}else{
								redirect(base_url().'dashboard/mark_attendance');
							}
					}else{
						$this->session->set_flashdata('message', array('info' => 'Invalid User', 'class' => 'danger'));
						redirect(base_url().'login');
					}
					
			}else{
					$this->load->view('auth',$data);
			}
	}
	
	public function logout(){
			if(!empty($this->session->userdata('email'))){
				$this->session->sess_destroy();
				$this->session->set_flashdata('message', array('info' => 'Logout Successfully', 'class' => 'danger'));
			}else{}
			$this->load->view('login');
	}
	
	
}
