<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
			parent::__construct();
			$this->load->model('user_model');
	}
	
	public function index()
	{
		if($this->session->userdata('role') == 1){
			redirect(base_url().'dashboard/list_attendance');
		}elseif($this->session->userdata('role') == 2){
			redirect(base_url().'dashboard/mark_attendance');
		}
		$this->load->view('login');
	}
	
	public function register()
	{
			$data['hod_users'] = $this->user_model->get_all();
			if ($this->input->server('REQUEST_METHOD') == 'POST'){
					$this->load->library('form_validation');
					$this->form_validation->set_rules('role', 'Role', 'required|trim');
					if($this->input->post('role') == 2){
							$this->form_validation->set_rules('hod', 'HOD', 'required|trim');
					}
					$this->form_validation->set_rules('name', 'Name', 'required|trim');
					$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]');
					$this->form_validation->set_rules('mobile', 'Mobile No', 'required|trim|max_length[12]|is_unique[users.mobile]');
					$this->form_validation->set_rules('profile_img', 'Profile Image', 'trim');
					$this->form_validation->set_rules('password', 'Password', 'required|trim');
					$this->form_validation->set_rules('passconf', 'Confirm Password', 'required|trim|matches[password]');
				
					if ($this->form_validation->run() == TRUE){
							if(!empty($_FILES['profile_img']['name'])){
									$config['upload_path']          = './uploads/';
									$config['allowed_types']        = 'gif|jpg|jpeg|png';
									$config['max_size']             = 2048;
									$config['max_width']            = 1024;
									$config['max_height']           = 768;

									$this->load->library('upload', $config);
									if (! $this->upload->do_upload('profile_img')){
											$upload_error = array('error' => $this->upload->display_errors());
									}else {
											$image_data = array('upload_data' => $this->upload->data());
											$save['profile_img'] = $image_data['upload_data']['file_name'];
									}
							}
				
							$save['role'] = $this->input->post('role');
							$save['hod'] = $this->input->post('hod');
							$save['name'] = $this->input->post('name');
							$save['email'] = $this->input->post('email');
							$save['mobile'] = $this->input->post('mobile');
							$save['password'] = md5($this->input->post('password'));
							$save['created_at'] = date('Y-m-d H:i:s');
							$insert = $this->db->insert('users', $save);
							if($insert){
								$this->session->set_flashdata('message', array('info' => 'Sucessfully Registered', 'class' => 'success'));
								redirect(base_url().'login');
							}
					}
			}
			$this->load->view('register', $data);
	}
	
	
}
