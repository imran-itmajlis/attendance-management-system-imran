<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
			parent::__construct();
			check_login();
			$this->load->model('user_model');
			$this->load->model('attendance_model');
	}
	
	/* public function index()
	{
		$this->load->view('login');
	} */
	
	public function mark_attendance(){
			$user_id = $this->session->userdata('id');
			$data['user'] = $this->user_model->get($user_id);
			$data['mark'] = $this->user_model->get_mark($user_id);
			$data['page_title'] = 'Mark Attendance';
			$data['page'] = 'mark_attendance';
			$this->load->view('template', $data);
	}
	
	public function list_attendance(){
			$user_id = $this->session->userdata('id');
			$data['attendance'] = $this->attendance_model->get_all($user_id);
			$data['page_title'] = 'List Attendance for Approve';
			$data['page'] = 'list_attendance';
			$this->load->view('template', $data);
	}
	
	public function mark_in_out(){
			$result = $this->user_model->mark_in_out($_GET['user_id']);
			if($result == 'mark in'){
				$class="success";
				$message="Successfully Marked In";
			}elseif($result == 'mark out'){
				$class="success";
				$message="Successfully Marked Out";
			}elseif($result == 'both marked'){
				$class="warning";
				$message="Todays Marked Done";
			}
			$this->session->set_flashdata('message', array('info' => $message, 'class' => $class));
			redirect(base_url().'dashboard/mark_attendance');
			
	}
	
	
}
